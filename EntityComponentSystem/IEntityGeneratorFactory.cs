﻿using System;

namespace EntityComponentSystem
{
    public interface IEntityGeneratorFactory
    {
        Func<IEntity> NewGenerator();
    }
}