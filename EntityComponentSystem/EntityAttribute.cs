﻿using System;

namespace EntityComponentSystem
{
    public class EntityAttribute : Attribute
    {
        public readonly ushort Prefix;
        public readonly Type Generator;
        
        public EntityAttribute(ushort prefix, Type generator)
        {
            Prefix = prefix;
            Generator = generator;
        }
    }
}