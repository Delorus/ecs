﻿using System;

namespace EntityComponentSystem
{
    
    public interface IEntity
    {   
        uint GetId();
    }

    public static class EntityUtil
    {
        public const uint PrefixMask = 0xFFFF_0000;

        public const uint UniqueNumberMask = 0x0000_FFFF;

        public static uint ComputeId(ushort prefix, ushort uniqueNumer)
        {
            return (uint) ((prefix << 8) + uniqueNumer);
        }
    }
} 