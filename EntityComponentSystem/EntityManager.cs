﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EntityComponentSystem
{
    public class EntityManager
    {
        private readonly Dictionary<ushort, Func<IEntity>> _entityGenerators = new Dictionary<ushort, Func<IEntity>>();
        private readonly Dictionary<uint, IEntity> _entities = new Dictionary<uint, IEntity>();

        public void Scan()
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(d => d.GetTypes());
            foreach (var type in types)
            {
                var entityAttr = type.GetCustomAttribute<EntityAttribute>();

                if (entityAttr == null)
                {
                    continue;
                }

                var prefix = entityAttr.Prefix;
//                var count = 0;
//                Func<IEntity> generator = () =>
//                {
//                    count++;
//                    if (count == 0xFFFF)
//                    {
//                        throw new Exception("it will never happen");
//                    }
//                    
//                    return Activator.CreateInstance(type, new object[] { count })
//                };
                _entityGenerators.Add(prefix, null);
            }
        }

        public IEntity TryNewEntity(ushort prefix)
        {
            if (!_entityGenerators.ContainsKey(prefix))
            {
                return null;
            }

            var entity = _entityGenerators[prefix]();
            _entities.Add(entity.GetId(), entity);

            return entity;
        }
    }
}