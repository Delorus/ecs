﻿using System;
using EntityComponentSystem;

namespace Tests
{
    [Entity(Prefix, typeof(FrogGeneratorFactory))]
    public class Frog : IEntity
    {
        public const ushort Prefix = 0x0001;

        private class FrogGeneratorFactory : IEntityGeneratorFactory
        {
            public Func<IEntity> NewGenerator()
            {
                ushort count = 0;
                return () =>
                {
                    count++;
                    if (count == 0xFFFF)
                    {
                        throw new Exception("it will never happen");
                    }

                    return new Frog(count);
                };
            }
        }

        private readonly uint _id;

        public Frog(ushort id)
        {
            _id = EntityUtil.ComputeId(Prefix, id);
        }

        public uint GetId()
        {
            return _id;
        }
    }
}