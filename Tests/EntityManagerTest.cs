using System;
using EntityComponentSystem;
using Xunit;
using static Xunit.Assert;

namespace Tests
{
    public class EntityManagerTest
    {

        private readonly EntityManager _entityManager;
        
        public EntityManagerTest()
        {
            _entityManager = new EntityManager();
            _entityManager.Scan();
        }

        [Fact(DisplayName = "New entities have unique id")]
        public void NewEntityTest()
        {
            var frog1 = _entityManager.TryNewEntity(Frog.Prefix);
            var frog2 = _entityManager.TryNewEntity(Frog.Prefix);
            var fox1 = _entityManager.TryNewEntity(Fox.Prefix);

            NotEqual(frog1.GetId(), frog2.GetId());
            Equal(frog1.GetId(), (uint) 257);
            Equal(frog2.GetId(), (uint) 258);
            Equal(fox1.GetId(), (uint) 513);
        }

        [Fact(DisplayName = "Cannot create entities more than ushort max value")]
        public void UniqueNumberOverflowErrorTest()
        {
            for (var i = 0; i < ushort.MaxValue - 1; i++)
            {
                _entityManager.TryNewEntity(Frog.Prefix);
            }

            Throws<Exception>(() => _entityManager.TryNewEntity(Frog.Prefix));
        }

        [Fact(DisplayName = "Entity manager return null value if can not find prefix")]
        public void ReturnNullIfNotContainsPrefix()
        {
            var entity = _entityManager.TryNewEntity(42);

            Null(entity);
        }
    }
}