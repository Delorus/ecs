﻿using System;
using EntityComponentSystem;

namespace Tests
{
    
    [Entity(Prefix, typeof(FoxGeneratorFactory))]
    public class Fox : IEntity
    {
        public const ushort Prefix = 0x0002;
        
        private class FoxGeneratorFactory : IEntityGeneratorFactory
        {
            public Func<IEntity> NewGenerator()
            {
                ushort count = 0;
                return () =>
                {
                    count++;
                    if (count == 0xFFFF)
                    {
                        throw new Exception("it will never happen");
                    }

                    return new Fox(count);
                };
            }
        }
        
        private readonly uint _id;

        public Fox(ushort id)
        {
            _id = EntityUtil.ComputeId(Prefix, id);
        }

        public uint GetId()
        {
            return _id;
        }
    }
}